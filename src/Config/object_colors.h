#ifndef OBJECT_COLORS_H
#define OBJECT_COLORS_H

enum class ObjectColors { RED_OBJ, BLUE_OBJ, GREEN_OBJ, YELLOW_OBJ };

#endif // !OBJECT_COLORS_H