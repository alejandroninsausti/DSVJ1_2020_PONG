#include "menu.h"

#include "raylib.h"

#include "utility.h"
#include "Config/screen_size.h"
#include "Scenes/Game Loop/game.h"
#include "Scenes/Game Loop/Player/player.h"

namespace pong
{
	namespace menu
	{
		bool OptionClicked(Rectangle textBox)
		{
			return CheckCollisionPointRec(GetMousePosition(), textBox);
		}

		namespace menu_credits
		{
			const int letterSize = 30;
			const int letterSpacing = 3;

			Rectangle textBoxExit =
			{ windowWidth / 2 - letterSize, (windowHeight / 10) * 9.5f, letterSize * 2.5f, letterSize + 5 };

			void Draw()
			{
				BeginDrawing();

				//draw library credits
				const Rectangle libraryTextbox =
				{
					0, //x position
					windowHeight - windowHeight / 10 * 9, //y position
					letterSize * 20, //width
					letterSize //height
				};
				DrawTextRec(GetFontDefault(), "Library used: RayLib", libraryTextbox, letterSize, letterSpacing, true, GRAY);

				//draw graphics motor credits
				const Rectangle motorTextbox =
				{
					0, //x position
					windowHeight - windowHeight / 10 * 8, //y position
					letterSize * 26, //width
					letterSize //height
				};
				DrawTextRec(GetFontDefault(), "Graphic Motor used: OpenGL", motorTextbox, letterSize, letterSpacing, true, GRAY);

				//draw sound editor credits
				const Rectangle soundTextbox =
				{
					0, //x position
					windowHeight - windowHeight / 10 * 7, //y position
					letterSize * 30, //width
					letterSize //height
				};
				DrawTextRec(GetFontDefault(), "Sound Recorder used: Audacity", soundTextbox, letterSize, letterSpacing, true, GRAY);

				//draw start of music credits
				const Rectangle musicTextbox =
				{
					0, //x position
					windowHeight - windowHeight / 10 * 6, //y position
					letterSize * 25, //width
					letterSize //height
				};
				DrawTextRec(GetFontDefault(), "Music used:", musicTextbox, letterSize, letterSpacing, true, GRAY);
				
				//draw LittleIdea credits
				const Rectangle littleIdeaTextbox =
				{
					0, //x position
					windowHeight - windowHeight / 10 * 5.5f, //y position
					letterSize * 26, //width
					letterSize //height
				};
				DrawTextRec(GetFontDefault(), "'Little Idea', by Bensound", littleIdeaTextbox, letterSize, letterSpacing, true, GRAY);

				//draw LittleIdea download link
				const Rectangle littleIdeaLinkTextbox =
				{
					0, //x position
					windowHeight - windowHeight / 10 * 5, //y position
					letterSize * 79 / 2, //width
					letterSize / 2 //height
				};
				DrawTextRec(GetFontDefault(), "(downloaded in https://www.bensound.com/bensound-music/bensound-littleidea.mp3)", littleIdeaLinkTextbox, letterSize / 2, letterSpacing, true, GRAY);

				//draw author credits
				const Rectangle authorTextbox =
				{
					0, //x position
					windowHeight - windowHeight / 10 * 4, //y position
					letterSize * 26, //width
					letterSize //height
				};
				DrawTextRec(GetFontDefault(), "Made by Alejandro Insausti", authorTextbox, letterSize, letterSpacing, true, GRAY);
				
				//draw exit option
				DrawRectangleRec(textBoxExit, GRAY);
				const Rectangle exitTextbox =
				{
					textBoxExit.x + letterSize / 5, //x position
					textBoxExit.y, //y position
					letterSize * 4, //width
					letterSize //height
				};
				DrawTextRec(GetFontDefault(), "Exit", exitTextbox, letterSize, letterSpacing, true, DARKGRAY);
				
				ClearBackground(DARKGRAY);
				EndDrawing();
			}

			void CreditsMenu()
			{
				do
				{
					UpdateMusicStream(pong::music);
					Draw();
				} while (IsKeyUp(KEY_ESCAPE) &&
					(IsMouseButtonUp(MOUSE_LEFT_BUTTON) || !OptionClicked(textBoxExit)));
			}
		}

		namespace menu_settings
		{
			enum class UnavailableKeys { UP = KEY_UP, DOWN = KEY_DOWN, LEFT = KEY_LEFT, RIGHT = KEY_RIGHT };
			const int letterSize = 30;
			const int letterSpacing = 3;
			bool soundIsOn = true;

			struct PlayerTextboxes
			{
				Rectangle textBoxUpKey = { 0, 0, 0, 0 };
				Rectangle textBoxDownKey = { 0, 0, 0, 0 };
				Rectangle textBoxAI = { 0, 0, 0, 0 };
				Rectangle textBoxPaddleColor = { 0, 0, 0, 0 };
			};

			PlayerTextboxes player1Boxes =
			{
				//textBoxUpKey
				{(windowWidth / 10) * 0.5f, (windowHeight / 10) * 4, letterSize * 2, letterSize * 2},
				//textBoxDownKey
				{(windowWidth / 10) * 0.5f, (windowHeight / 10) * 8, letterSize * 2, letterSize * 2},
				//textBoxAI
				{(windowWidth / 10) * 2.5f, (windowHeight / 10) * 5, letterSize * 2, letterSize * 2},
				//textBoxPaddleColor
				{(windowWidth / 10) * 2.5f, (windowHeight / 10) * 7, letterSize * 2, letterSize * 2}
			};

			PlayerTextboxes player2Boxes =
			{
				//textBoxUpKey
				{(windowWidth / 10) * 8.5f, (windowHeight / 10) * 4, letterSize * 2, letterSize * 2},
				//textBoxDownKey
				{(windowWidth / 10) * 8.5f, (windowHeight / 10) * 8, letterSize * 2, letterSize * 2},
				//textBoxAI
				{(windowWidth / 10) * 6.5f, (windowHeight / 10) * 5, letterSize * 2, letterSize * 2},
				//textBoxPaddleColor
				{(windowWidth / 10) * 6.5f, (windowHeight / 10) * 7, letterSize * 2, letterSize * 2}
			};

			Rectangle textBoxSound =
			{ windowWidth / 2 - letterSize, (windowHeight / 10) * 8.5f, letterSize * 2.5f, letterSize + 5 };

			Rectangle textBoxExit =
			{ windowWidth / 2 - letterSize, (windowHeight / 10) * 9.5f, letterSize * 2.5f, letterSize + 5 };

			//SETTINGS
			void SelectPlayerOption(Player& player, PlayerTextboxes textBoxes)
			{
				if (OptionClicked(textBoxes.textBoxUpKey))
				{
					player.controls.up = InputKey();
				}
				else if (OptionClicked(textBoxes.textBoxDownKey))
				{
					player.controls.down = InputKey();
				}
				else if (OptionClicked(textBoxes.textBoxAI))
				{
					player.isABot ? player.isABot = false : player.isABot = true;
				}
				else if (OptionClicked(textBoxes.textBoxPaddleColor))
				{
					switch (player.currentColor)
					{
					case ObjectColors::RED_OBJ:
						player.currentColor = ObjectColors::BLUE_OBJ;
						player.color = BLUE;
						break;
					case ObjectColors::BLUE_OBJ:
						player.currentColor = ObjectColors::GREEN_OBJ;
						player.color = GREEN;
						break;
					case ObjectColors::GREEN_OBJ:
						player.currentColor = ObjectColors::YELLOW_OBJ;
						player.color = YELLOW;
						break;
					case ObjectColors::YELLOW_OBJ:
						player.currentColor = ObjectColors::RED_OBJ;
						player.color = RED;
						break;
					}
				}
				else if (OptionClicked(textBoxSound))
				{
					if (soundIsOn)
					{
						soundIsOn = false;
						SetMasterVolume(0);
					}
					else
					{
						soundIsOn = true;
						SetMasterVolume(0.5f);
					}
				}
			}

			void DrawKeyMappingOptions(Rectangle textBox, bool upKey, int currentKey)
			{
				Rectangle optionNameTextBox = textBox;
				optionNameTextBox.width += textBox.width * 2;
				optionNameTextBox.x -= textBox.width / 2;
				optionNameTextBox.height += textBox.height / 2;
				optionNameTextBox.y -= textBox.height / 2;
				DrawRectangleRec(textBox, GRAY); //draw option box
				upKey ?
					DrawTextRec(GetFontDefault(), "UP Key", optionNameTextBox, letterSize, letterSpacing, false, GRAY) :
					DrawTextRec(GetFontDefault(), "DOWN Key", optionNameTextBox, letterSize, letterSpacing, false, GRAY);
				switch ((UnavailableKeys)currentKey)
				{
				case UnavailableKeys::UP:
				case UnavailableKeys::DOWN:
				case UnavailableKeys::LEFT:
				case UnavailableKeys::RIGHT:
					break;
				default:
					DrawTextCodepoint
					(GetFontDefault(), currentKey, { textBox.x + textBox.width / 5, textBox.y }, 5, DARKGRAY);
					break;
				}
			}

			void DrawBotOption(Rectangle textBox, Color boxColor, bool isABot)
			{
				const Rectangle isABotTextbox =
				{
					textBox.x, //x position
					textBox.y + textBox.height / 3, //y position
					letterSize * 3, //width
					letterSize //height
				};

				const Rectangle isNotABotTextbox =
				{
					textBox.x + textBox.width / 5, //x position
					textBox.y + textBox.height / 3, //y position
					letterSize * 3, //width
					letterSize //height
				};

				const Rectangle optionNameTextbox =
				{
					textBox.x - letterSize * 3, //x position
					textBox.y - letterSize, //y position
					letterSize * 16, //width
					letterSize //height
				};

				DrawRectangleRec(textBox, boxColor); //draw option box
				isABot ?
					DrawTextRec(GetFontDefault(), "YES", isABotTextbox, letterSize, letterSpacing, true, DARKGREEN) :
					DrawTextRec(GetFontDefault(), "NO", isNotABotTextbox, letterSize, letterSpacing, true, DARKPURPLE);
				DrawTextRec(GetFontDefault(), "Controlled by PC", optionNameTextbox, letterSize, letterSpacing, true, GRAY);
			}

			void DrawColorOption(Rectangle textBox, ObjectColors paddleColor, Color boxColor)
			{
				const Rectangle optionNameTextbox =
				{
					textBox.x - letterSize * 2, //x position
					textBox.y - letterSize, //y position
					letterSize * 12, //width
					letterSize //height
				};

				DrawTextRec(GetFontDefault(), "Paddle Color", optionNameTextbox, letterSize, letterSpacing, true, GRAY);
				DrawRectangleRec(textBox, boxColor); //draw option box

				const Rectangle colorTextbox =
				{
					textBox.x + textBox.height / 3, //x position
					textBox.y + textBox.height / 3, //y position
					letterSize, //width
					letterSize //height
				};
				switch (paddleColor)
				{
				case ObjectColors::RED_OBJ:
					DrawTextRec(GetFontDefault(), "R", colorTextbox, letterSize, letterSpacing, true, DARKPURPLE);
					break;
				case ObjectColors::BLUE_OBJ:
					DrawTextRec(GetFontDefault(), "B", colorTextbox, letterSize, letterSpacing, true, DARKBLUE);
					break;
				case ObjectColors::GREEN_OBJ:
					DrawTextRec(GetFontDefault(), "G", colorTextbox, letterSize, letterSpacing, true, DARKGREEN);
					break;
				case ObjectColors::YELLOW_OBJ:
					DrawTextRec(GetFontDefault(), "Y", colorTextbox, letterSize, letterSpacing, true, ORANGE);
					break;
				}
			}

			void DrawPlayerOptions(PlayerTextboxes textBoxes, Player player)
			{
				//----------------------------------draw up-key mapping
				DrawKeyMappingOptions(textBoxes.textBoxUpKey, true, player.controls.up);
				DrawKeyMappingOptions(textBoxes.textBoxDownKey, false, player.controls.down);

				//----------------------------------draw activate AI button
				player.isABot ?
					DrawBotOption(textBoxes.textBoxAI, GREEN, player.isABot) :
					DrawBotOption(textBoxes.textBoxAI, RED, player.isABot);

				//----------------------------------draw paddel color
				DrawColorOption(textBoxes.textBoxPaddleColor, player.currentColor, player.color);
			}

			void Draw()
			{
				BeginDrawing();

				DrawPlayerOptions(player1Boxes, gameloop::player::player1); //draw all player 1 options
				DrawPlayerOptions(player2Boxes, gameloop::player::player2); //draw all player 2 options

				//divide screen between players
				DrawRectangle(windowWidth / 2, 0, 20, windowHeight, GRAY);

				const Rectangle player1Textbox =
				{
					windowWidth / 10, //x position
					windowHeight / 10, //y position
					letterSize * 8 * 3, //width
					letterSize * 3 //height
				};

				const Rectangle player2Textbox =
				{
					windowWidth / 10 * 5.5f, //x position
					windowHeight / 10, //y position
					letterSize * 8 * 3, //width
					letterSize * 3 //height
				};

				DrawTextRec(GetFontDefault(), "Player 1", player1Textbox, letterSize * 3, letterSpacing, true, GRAY);
				DrawTextRec(GetFontDefault(), "Player 2", player2Textbox, letterSize * 3, letterSpacing, true, GRAY);

				//draw mute option
				DrawRectangleRec(textBoxSound, GRAY);
				const Rectangle soundTextbox =
				{
					textBoxSound.x + letterSize / 5, //x position
					textBoxSound.y, //y position
					letterSize * 4, //width
					letterSize //height
				};
				DrawTextRec(GetFontDefault(), "Mute", soundTextbox, letterSize, letterSpacing, true, DARKGRAY);

				//draw exit option
				DrawRectangleRec(textBoxExit, GRAY);
				const Rectangle exitTextbox =
				{
					textBoxExit.x + letterSize / 5, //x position
					textBoxExit.y, //y position
					letterSize * 4, //width
					letterSize //height
				};
				DrawTextRec(GetFontDefault(), "Exit", exitTextbox, letterSize, letterSpacing, true, DARKGRAY);
				
				ClearBackground(DARKGRAY);
				EndDrawing();
			}

			void SettingsMenu()
			{
				do
				{
					UpdateMusicStream(pong::music);

					//Input | Update
					if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
					{
						//activates the textboxes of one player or another based on where the mouse clicked
						GetMouseX() > windowWidth / 2 ?
							SelectPlayerOption(gameloop::player::player2, player2Boxes) :
							SelectPlayerOption(gameloop::player::player1, player1Boxes);
					}

					//Draw
					Draw();
				} while (IsKeyUp(KEY_ESCAPE) &&
					(IsMouseButtonUp(MOUSE_LEFT_BUTTON) || !OptionClicked(textBoxExit)));
			}
		}

		namespace menu_main
		{
			const int letterSize = 50;
			const int letterSpacing = 5;

			Rectangle textBoxPlay =
			{
				windowWidth / 2 - letterSize, //x position
				windowHeight / 2 - letterSize * 2, //y position
				letterSize * 2.5f, //width
				letterSize + 5 //height
			};

			Rectangle textBoxSettings =
			{
				windowWidth / 2 - 2 * letterSize, //x position
				windowHeight / 2, //y position
				letterSize * 4.5f, //width
				letterSize + 5 //height
			};

			Rectangle textBoxCredits =
			{
				windowWidth / 2 - 1.75 * letterSize, //x position
				windowHeight / 2 + letterSize * 2, //y position
				letterSize * 4, //width
				letterSize + 5 //height
			};

			Rectangle textBoxExit =
			{
				windowWidth / 2 - letterSize, //x position
				windowHeight / 2 + letterSize * 4, //y position
				letterSize * 2.5f, //width
				letterSize + 5 //height
			};

			void DrawOptions()
			{
				//draw play option
				DrawRectangleRec(textBoxPlay, GRAY);
				Rectangle playOptionTextbox =
				{
					textBoxPlay.x + letterSize / 5, //x position
					textBoxPlay.y, //y position
					letterSize * 4, //width
					letterSize //height
				};
				DrawTextRec(GetFontDefault(), "Play", playOptionTextbox, letterSize, letterSpacing, true, DARKGRAY);

				//draw settings option
				DrawRectangleRec(textBoxSettings, GRAY);
				Rectangle settingsOptionTextbox =
				{
					textBoxSettings.x + letterSize / 9, //x position
					textBoxSettings.y, //y position
					letterSize * 8, //width
					letterSize //height
				};
				DrawTextRec(GetFontDefault(), "Settings", settingsOptionTextbox, letterSize, letterSpacing, true, DARKGRAY);

				//draw credits option
				DrawRectangleRec(textBoxCredits, GRAY);
				Rectangle creditsOptionTextbox =
				{
					textBoxCredits.x + letterSize / 8, //x position
					textBoxCredits.y, //y position
					letterSize * 7, //width
					letterSize //height
				};
				DrawTextRec(GetFontDefault(), "Credits", creditsOptionTextbox, letterSize, letterSpacing, true, DARKGRAY);

				//draw exit option
				DrawRectangleRec(textBoxExit, GRAY);
				Rectangle exitOptionTextbox =
				{
					textBoxExit.x + letterSize / 5, //x position
					textBoxExit.y, //y position
					letterSize * 8, //width
					letterSize //height
				};
				DrawTextRec(GetFontDefault(), "Exit", exitOptionTextbox, letterSize, letterSpacing, true, DARKGRAY);
			}

			void Draw()
			{
				BeginDrawing();
				//draw title
				DrawText("Pong", static_cast<int>(windowWidth / 4), 0, 200, GRAY);
				//draw play, settings and exit options
				DrawOptions();
				ClearBackground(DARKGRAY);
				EndDrawing();
			}

			void MainMenu()
			{
				do
				{
					UpdateMusicStream(pong::music);

					//Input | Update
					if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON))
					{
						if (OptionClicked(textBoxPlay))
						{
							gameloop::Game();
						}
						else if (OptionClicked(textBoxSettings))
						{
							menu_settings::SettingsMenu();
						}
						else if (OptionClicked(textBoxCredits))
						{
							menu_credits::CreditsMenu();
						}
						else if (OptionClicked(textBoxExit))
						{
							CloseWindow();
						}
					}

					//Draw
					Draw();
				} while (IsKeyUp(KEY_ESCAPE));
			}
		}
	}
}