#include "ball.h"

#include "Scenes/Game Loop/Player/player.h"
#include "Config/screen_size.h"
#include "utility.h"

namespace pong
{
	namespace gameloop
	{
		namespace ball
		{
			const short totalBalls = 5;
			Ball ball[totalBalls];
			short activeBalls = 1;
			Sound bounceSound1 = LoadSound("../res/assets/sound/bounce_sound_1.mp3");
			Sound bounceSound2 = LoadSound("../res/assets/sound/bounce_sound_2.mp3");
			Sound scorePointSound = LoadSound("../res/assets/sound/point_scored_sound.mp3");

			bool IsMovingWithinMap(float ballPosition, float ballRadius, float screenLimit)
			{
				//check if the ball's position after moving will be inside of the map
				return ballPosition - ballRadius > 0
					&& ballPosition + ballRadius < screenLimit;
			}

			Ball Generate(Vector2 ballPivot, Vector2 ballInitialDirection)
			{
				Ball ball;

				//assign ball's position
				ball.position = ballPivot;

				//assign the direction in which the ball will be launched when the Game begins
				ball.speed = ballInitialDirection;

				//return
				return ball;
			}

			void BounceAgainstPlayer(Ball& ball, Player player)
			{
				if (CheckCollisionCircleRec(ball.position, ball.radius, player.body))
				{
					float ballMaxSpeed = 30;

					//add the speed of the player to the speed of the ball but only if it won't exceed the speed limit
					if (GetAbsoluteValue(ball.speed.y + player.speed) <= ballMaxSpeed)
					{
						ball.speed.y += player.speed / 2;
					}

					//add 0.5f to the ball's speed, adding it to the current speed direction (-5 + -0.5f || 5 + 0.5f)
					//but only if it won't exceed the speed limit
					if (GetAbsoluteValue(ball.speed.x) / (ball.speed.x * 10) <= ballMaxSpeed)
					{
						ball.speed.x += GetAbsoluteValue(ball.speed.x) / (ball.speed.x * 2);
					}

					//invert speed direction
					ball.speed.x *= -1;

					//bounce ball so it doesn't get stucked in paddle
					ball.position.x += ball.speed.x;

					GetRandomValue(0, 1) ? PlaySound(bounceSound1) : PlaySound(bounceSound2);
				}
			}

			void Move(Ball& ball)
			{
				ball.position.x > windowWidth / 2 ?
					BounceAgainstPlayer(ball, player::player2) : BounceAgainstPlayer(ball, player::player1);

				Ball ballAfterMoving = ball;
				ballAfterMoving.position.x += ball.speed.x;
				ballAfterMoving.position.y += ball.speed.y;

				if (!IsMovingWithinMap(ballAfterMoving.position.y, ballAfterMoving.radius, windowHeight))
				{
					ball.speed.y *= -1;
				}
				ball.position.y += ball.speed.y;
				ball.position.x += ball.speed.x;
			}

			void ScorePoint(Ball& ball)
			{
				if (!(IsMovingWithinMap(ball.position.x, ball.radius, windowWidth)))
				{
					ball.position.x > windowWidth / 2 ? player::player1.score++ : player::player2.score++;

					ball.speed.x = 5;
					ball.speed.y = 5;
					ball.position.x = windowWidth / 2;
					ball.position.y = windowHeight / 2;

					PlaySound(scorePointSound);
				}
			}

			void DeInit()
			{
				activeBalls = 1;
				ball[0] = ball::Generate({ windowWidth / 2, windowHeight / 2 }, { 5, 5 });
			}
		};
	}
}