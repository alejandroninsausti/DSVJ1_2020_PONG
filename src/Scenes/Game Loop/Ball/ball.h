#ifndef BALL_H
#define BALL_H

#include "raylib.h"

#include "ball_entity.h"

namespace pong
{
	namespace gameloop
	{
		namespace ball
		{
			extern Ball ball[];
			extern short activeBalls;

			Ball Generate(Vector2 ballPivot, Vector2 ballInitialDirection);
			void Move(Ball& ball);
			void ScorePoint(Ball& ball);
			void DeInit();
		}
	}
}

#endif // !BALL_H

