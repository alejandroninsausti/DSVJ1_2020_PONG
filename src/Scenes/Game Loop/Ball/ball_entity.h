#ifndef BALL_ENTITY_H
#define BALL_ENTITY_H

#include "raylib.h"

struct Ball
{
	//Vector2 name = {x, y}; (VECTOR2 TEMPLATE)

	Vector2 position = { 0, 0 };
	float radius = 10;
	Vector2 speed = { 0, 0 };
	Color color = RED;
};

#endif // !BALL_ENTITY_H