#ifndef GAME_H
#define GAME_H

#include "raylib.h"

namespace pong
{
	extern const Music music;

	void StartGame();
	namespace gameloop
	{
		void Game();
	}
}

#endif // !GAME_H
