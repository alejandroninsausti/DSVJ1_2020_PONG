#include "powerups.h"

#include "raylib.h"

#include "Scenes/Game Loop/Player/player.h"
#include "Scenes/Game Loop/Ball/ball.h"
#include "Config/screen_size.h"

namespace pong
{
	namespace gameloop
	{
		namespace powerups
		{
			enum class Types { SHIELD, BIGGER_PADDLE, FASTER_PADDLE, MULTIBALL, OBSTACLE, INVERT_SPEED, INVERT_KEYS };

			const short maxEffects = 5;

			struct PowerUpEffect
			{
				Types type = Types::OBSTACLE;
				short durationCounter = 0;
				short indexOfCollidedBall = -1;
				bool wasHittedByRightPlayer = false;
			};

			struct PowerUpBox
			{
				Rectangle box = { 0, 0, 50, 50 };
				Types type = Types::OBSTACLE;
				bool isOnScreen = false;
				short activeEffects = 0;
				PowerUpEffect effects[maxEffects];
			};

			PowerUpBox box;
			short totalScoreCounter = 0;
			const short effectMaxDuration = 750;
			Player playerTemplate;

			Sound turnOnPowerUp = LoadSound("../res/assets/sound/power_up_sound.mp3");
			Sound turnOffPowerUp = LoadSound("../res/assets/sound/power_down_sound.mp3");

			void DeactivateEffect(short effectIndex)
			{
				box.effects[effectIndex] = box.effects[box.activeEffects - 1];
				box.effects[effectIndex].durationCounter = 0;
				box.activeEffects--;

				PlaySound(turnOffPowerUp);
			}

			bool CollisionShieldBall(bool leftShield, short ballIndex)
			{
				if (leftShield)
				{
					return CheckCollisionPointCircle
					(
						{ 20, ball::ball[ballIndex].position.y },
						ball::ball[ballIndex].position,
						ball::ball[ballIndex].radius
					);
				}
				else
				{
					return CheckCollisionPointCircle
					(
						{ windowWidth - 20, ball::ball[ballIndex].position.y },
						ball::ball[ballIndex].position,
						ball::ball[ballIndex].radius
					);
				}
			}

			void Shield(short effectIndex)
			{
				for (short i = 0; i < ball::activeBalls; i++)
				{
					if (box.effects[effectIndex].wasHittedByRightPlayer && CollisionShieldBall(false, i))
					{
						ball::ball[i].speed.x *= -1;
						DeactivateEffect(effectIndex);
					}
					else if (!box.effects[effectIndex].wasHittedByRightPlayer && CollisionShieldBall(true, i))
					{
						ball::ball[i].speed.x *= -1;
						DeactivateEffect(effectIndex);
					}
				}
			}

			void BiggerPaddle(short effectIndex)
			{
				box.effects[effectIndex].wasHittedByRightPlayer ?
					player::player2.body.height = playerTemplate.body.height + playerTemplate.body.height / 2 :
					player::player1.body.height = playerTemplate.body.height + playerTemplate.body.height / 2;

				if (player::player1.score + player::player2.score != totalScoreCounter)
				{
					box.effects[effectIndex].wasHittedByRightPlayer ?
						player::player2.body.height = playerTemplate.body.height :
						player::player1.body.height = playerTemplate.body.height;
					DeactivateEffect(effectIndex);
				}
			}

			void FasterPaddle(short effectIndex)
			{
				box.effects[effectIndex].wasHittedByRightPlayer ?
					player::player2.maxSpeed = playerTemplate.maxSpeed + playerTemplate.maxSpeed / 2 :
					player::player1.maxSpeed = playerTemplate.maxSpeed + playerTemplate.maxSpeed / 2;

				if (player::player1.score + player::player2.score != totalScoreCounter)
				{
					box.effects[effectIndex].wasHittedByRightPlayer ?
						player::player2.maxSpeed = playerTemplate.maxSpeed :
						player::player1.maxSpeed = playerTemplate.maxSpeed;
					DeactivateEffect(effectIndex);
				}
			}

			void Multiball(short effectIndex)
			{
				if (box.effects[effectIndex].durationCounter == 0)
				{
					ball::ball[ball::activeBalls] = ball::ball[box.effects[effectIndex].indexOfCollidedBall];
					ball::ball[ball::activeBalls].speed.y *= -1;
					ball::activeBalls++;
					box.effects[effectIndex].durationCounter++;
				}
				else if (box.effects[effectIndex].durationCounter <= effectMaxDuration)
				{
					box.effects[effectIndex].durationCounter++;
				}
				else
				{
					ball::activeBalls--;
					DeactivateEffect(effectIndex);
				}
			}

			void InvertSpeed(short effectIndex)
			{
				if (box.effects[effectIndex].durationCounter == 0)
				{
					for (short i = 0; i < ball::activeBalls; i++)
					{
						ball::ball[i].speed.x *= -1;
						ball::ball[i].speed.y *= -1;
					}
					box.effects[effectIndex].durationCounter++;
				}
				else if (box.effects[effectIndex].durationCounter <= effectMaxDuration)
				{
					box.effects[effectIndex].durationCounter++;
				}
				else
				{
					for (short i = 0; i < ball::activeBalls; i++)
					{
						ball::ball[i].speed.x *= -1;
						ball::ball[i].speed.y *= -1;
					}
					DeactivateEffect(effectIndex);
				}
			}

			void InvertKeys(short effectIndex)
			{
				if (box.effects[effectIndex].durationCounter == 0)
				{
					KeyBindings keyBindingsPlayer1 = player::player1.controls;
					KeyBindings keyBindingsPlayer2 = player::player2.controls;
					player::player1.controls.up = keyBindingsPlayer1.down;
					player::player1.controls.down = keyBindingsPlayer1.up;
					player::player2.controls.up = keyBindingsPlayer2.down;
					player::player2.controls.down = keyBindingsPlayer2.up;
					box.effects[effectIndex].durationCounter++;
				}
				else if (box.effects[effectIndex].durationCounter <= effectMaxDuration)
				{
					box.effects[effectIndex].durationCounter++;
				}
				else
				{
					KeyBindings keyBindingsPlayer1 = player::player1.controls;
					KeyBindings keyBindingsPlayer2 = player::player2.controls;
					player::player1.controls.up = keyBindingsPlayer1.down;
					player::player1.controls.down = keyBindingsPlayer1.up;
					player::player2.controls.up = keyBindingsPlayer2.down;
					player::player2.controls.down = keyBindingsPlayer2.up;
					DeactivateEffect(effectIndex);
				}
			}

			void Manager()
			{
				//check if the ball hitted the power up and make it dissapear
				if (box.isOnScreen)
				{
					for (short i = 0; i < ball::activeBalls; i++)
					{
						if (CheckCollisionCircleRec(ball::ball[i].position, ball::ball[i].radius, box.box))
						{
							if (box.type != Types::OBSTACLE)
							{
								PlaySound(turnOnPowerUp);
								box.effects[box.activeEffects].type = box.type;
								box.effects[box.activeEffects].indexOfCollidedBall = i;
								ball::ball[i].speed.x < 0 ?
									box.effects[box.activeEffects].wasHittedByRightPlayer = true :
									box.effects[box.activeEffects].wasHittedByRightPlayer = false;
								box.activeEffects++;
							}
							else
							{
								//make the ball bounce in the X or Y depending on if 
								//it hitted the obstacle in the top/bottom or in the left side/right side
								float distanceBallBoxX = ball::ball[i].position.x - (box.box.x + box.box.width / 2);
								float distanceBallBoxY = ball::ball[i].position.y - (box.box.y + box.box.height / 2);
								distanceBallBoxX > distanceBallBoxY ?
									ball::ball[i].speed.x *= -1 :
									ball::ball[i].speed.y *= -1;
							}

							box.isOnScreen = false;
						}
					}
				}
				else if (box.activeEffects < maxEffects && GetRandomValue(1, 100) == 1)
				{
					box.box.x = static_cast<float>(GetRandomValue(windowWidth / 10 * 2, windowWidth / 10 * 8));
					box.box.y = static_cast<float>(GetRandomValue(windowHeight / 10 * 2, windowHeight / 10 * 8));
					box.type = (Types)GetRandomValue(0, 6);
					box.isOnScreen = true;
				}

				for (short i = 0; i < box.activeEffects; i++)
				{
					switch (box.effects[i].type)
					{
					case Types::SHIELD:
						Shield(i);
						break;
					case Types::BIGGER_PADDLE:
						BiggerPaddle(i);
						break;
					case Types::FASTER_PADDLE:
						FasterPaddle(i);
						break;
					case Types::MULTIBALL:
						Multiball(i);
						break;
					case Types::INVERT_SPEED:
						InvertSpeed(i);
						break;
					case Types::INVERT_KEYS:
						InvertKeys(i);
						break;
					default:
						break;
					}
				}

				totalScoreCounter = player::player1.score + player::player2.score;
			}

			void DrawPowerUpBox()
			{
				if (box.isOnScreen)
				{
					switch (box.type)
					{
					case Types::SHIELD:
						DrawRectangleRec(box.box, DARKBLUE); //blue shield
						break;
					case Types::BIGGER_PADDLE:
						DrawRectangleRec(box.box, BROWN); //brown big paddle
						break;
					case Types::FASTER_PADDLE:
						DrawRectangleRec(box.box, YELLOW); // yellow speedy paddle
						break;
					case Types::MULTIBALL:
						DrawRectangleRec(box.box, RED); //red multiball
						break;
					case Types::INVERT_SPEED:
						DrawRectangleRec(box.box, SKYBLUE); //skyblue invert speed
						break;
					case Types::INVERT_KEYS:
						DrawRectangleRec(box.box, GREEN); //green inverted keys
						break;
					default:
						DrawRectangleRec(box.box, DARKGRAY); //gray obstacle
						break;
					}
				}
			}

			void DrawPowerUpEffects()
			{
				for (short i = 0; i < box.activeEffects; i++)
				{
					switch (box.effects[i].type)
					{
					case Types::SHIELD:
						box.effects[i].wasHittedByRightPlayer ?
							DrawLine(windowWidth - 1, 0, windowWidth - 1, windowHeight, BLUE) :
							DrawLine(1, 0, 1, windowHeight, BLUE);
						break;
					case Types::BIGGER_PADDLE:
						break;
					case Types::FASTER_PADDLE:
						break;
					case Types::MULTIBALL:
						break;
					case Types::INVERT_SPEED:
						break;
					case Types::INVERT_KEYS:
						break;
					default:
						break;
					}
				}
			}
		}
	}
}