#ifndef POWERUPS_H
#define POWERUPS_H

namespace pong
{
	namespace gameloop
	{
		namespace powerups
		{
			void DrawPowerUpBox();
			void DrawPowerUpEffects();
			void Manager();
		}
	}
}

#endif // !POWERUPS_H