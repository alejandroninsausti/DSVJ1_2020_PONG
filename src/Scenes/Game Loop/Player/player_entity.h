#ifndef PLAYER_ENTITY_H
#define PLAYER_ENTITY_H

#include "raylib.h"

#include "Config/key_bindings.h"
#include "Config/object_colors.h"

struct Player
{
	//Vector2 name = {x, y};
	Rectangle body = { 0, 0, 25, 100 }; //set position x, y, width and heigth, in that order
	float maxSpeed = 10;
	float speed = 0;
	Color color = { 0, 0, 0, 0 };
	ObjectColors currentColor = (ObjectColors)0;
	KeyBindings controls = { 0, 0 };
	bool isABot = false;
	short score = 0;
	short winStreak = 0;
};

#endif // !PLAYER_ENTITY_H