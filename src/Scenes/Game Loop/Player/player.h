#ifndef PLAYER_H
#define PLAYER_H

#include "player_entity.h"

namespace pong
{
	namespace gameloop
	{
		namespace player
		{
			extern Player player1;
			extern Player player2;

			Player Generate(Vector2 playerPivot, ObjectColors playerColor, KeyBindings playerControls);
			void Move(Player& player);
			void DeInit(bool eraseWinStreak);
		}
	}
}

#endif // !PLAYER_H