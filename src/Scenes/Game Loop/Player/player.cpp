#include "player.h"

#include"Scenes/Game Loop/Ball/ball.h"
#include "Config/screen_size.h"
#include "utility.h"

namespace pong
{
	namespace gameloop
	{
		namespace player
		{
			Player player1 = player::Generate
			({ 0, windowHeight / 2 }, ObjectColors::RED_OBJ, { KEY_W, KEY_S });
			Player player2 = player::Generate
			({ windowWidth, windowHeight / 2 }, ObjectColors::BLUE_OBJ, { KEY_O, KEY_L });
						
			bool IsMovingWithinMap(Rectangle playerAfterMoving)
			{
				//check if the player's position after moving will be inside of the map
				return playerAfterMoving.y > 0 && playerAfterMoving.y + playerAfterMoving.height < windowHeight;
			}

			Player Generate(Vector2 playerPivot, ObjectColors playerColor, KeyBindings playerControls) 
			{
				Player player;

				//assign player's rectangle position
				player.body.x = playerPivot.x - player.body.width / 2;
				player.body.y = playerPivot.y - player.body.height / 2;

				//reset player's speed
				player.speed = 0;

				//assign player's color
				player.currentColor = playerColor;
				switch (playerColor)
				{
				case ObjectColors::RED_OBJ:
					player.color = RED;
					break;
				case ObjectColors::BLUE_OBJ:
					player.color = BLUE;
					break;
				case ObjectColors::GREEN_OBJ:
					player.color = GREEN;
					break;
				case ObjectColors::YELLOW_OBJ:
					player.color = YELLOW;
					break;
				}

				//assign player's control/keys
				player.controls = playerControls;

				//reset player's score
				player.score = 0;

				//return
				return player;
			}

			void Move(Player& player)
			{
				Rectangle playerAfterMoving = player.body;

				if (player.isABot)
				{
					Ball closestBall = ball::ball[0];
					for (short i = 0; i < ball::activeBalls; i++)
					{
						if (GetAbsoluteValue(closestBall.position.x - player.body.x)
					> GetAbsoluteValue(ball::ball[i].position.x - player.body.x))
						{
							closestBall = ball::ball[i];
						}
					}
					if (closestBall.position.y - closestBall.radius < player.body.y)
					{
						playerAfterMoving.y -= player.maxSpeed;

						if (IsMovingWithinMap(playerAfterMoving))
						{
							player.speed = -player.maxSpeed;
							player.body.y -= player.maxSpeed;
						}
					}
					else if (closestBall.position.y + closestBall.radius > player.body.y + player.body.height)
					{
						playerAfterMoving.y += player.maxSpeed;

						if (IsMovingWithinMap(playerAfterMoving))
						{
							player.speed = player.maxSpeed;
							player.body.y += player.maxSpeed;
						}
					}
					else
					{
						player.speed = 0;
					}
				}
				else
				{
					//move the player in the direction according to the key he pressed and modify
					//internal speed value (speed is used in collisions between players and ball)
					if (IsKeyDown(player.controls.up))
					{
						playerAfterMoving.y -= player.maxSpeed;

						if (IsMovingWithinMap(playerAfterMoving))
						{
							player.speed = -player.maxSpeed;
							player.body.y -= player.maxSpeed;
						}
					}
					else if (IsKeyDown(player.controls.down))
					{
						playerAfterMoving.y += player.maxSpeed;

						if (IsMovingWithinMap(playerAfterMoving))
						{
							player.speed = player.maxSpeed;
							player.body.y += player.maxSpeed;
						}
					}
					else
					{
						player.speed = 0;
					}
				}
			}

			void DeInit(bool eraseWinStreak)
			{
				player1.score = 0;
				player2.score = 0;
				if (eraseWinStreak)
				{
					player1.winStreak = 0;
					player2.winStreak = 0;
				}
			}
		}
	}
}
	

