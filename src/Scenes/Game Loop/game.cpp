/*
* Basic Pong. by Alejandro Insausti
*/

#include "game.h"

#include "raylib.h"

#include "Scenes/Game Loop/Player/player.h"
#include "Scenes/Game Loop/Ball/ball.h"
#include "Config/screen_size.h"
#include "utility.h"
#include "Scenes/Menu/menu.h"
#include "Scenes/Game Loop/Power-Ups/powerups.h"


namespace pong
{
	const Music music = LoadMusicStream("../res/assets/music/bensound_littleidea.mp3");

	void Init()
	{
		InitWindow(windowWidth, windowHeight, "Basic Pong v0.49"); //open OpenGL window
		InitAudioDevice(); //init audio device
		SetMasterVolume(1.0f); //set initial volume
		PlayMusicStream(music);
		SetTargetFPS(60); //set max frame rate
	}

	void DeInit()
	{
		CloseAudioDevice(); //close audio device
		CloseWindow(); //close Game
	}

	void StartGame()
	{
		Init();
		pong::menu::menu_main::MainMenu();
		DeInit();
	}

	namespace gameloop
	{		
		namespace ingame_menu
		{
			const int letterSize = 30;
			const float letterSpacing = 5;

			void WinMenu()
			{
				const Rectangle playerWonTextbox =
				{
					windowWidth / 2 - letterSize * 4.5f, //x position
					windowHeight / 2.25f, //y position
					letterSize * 14, //width
					letterSize //height
				};

				const Rectangle playAgainTextbox =
				{
					windowWidth / 2 - letterSize * 8.5f, //x position
					windowHeight / 2, //y position
					letterSize * 25, //width
					letterSize //height
				};

				//draw a text in middle of the screen which shows what player won
				player::player1.score > player::player2.score ?
					DrawTextRec(GetFontDefault(), "PLAYER ONE WON", playerWonTextbox, letterSize, letterSpacing, true, GRAY) :
					DrawTextRec(GetFontDefault(), "PLAYER TWO WON", playerWonTextbox, letterSize, letterSpacing, true, GRAY);

				//draw text explaining how to restart the game now thats it has ended
				DrawTextRec(GetFontDefault(), "PRESS SPACE TO PLAY AGAIN", playAgainTextbox, letterSize, letterSpacing, true, GRAY);
			}

			void PauseMenu()
			{
				const Rectangle resumeTextbox =
				{
					windowWidth / 2 - letterSize * 8.5f, //x position
					windowHeight / 2, //y position
					letterSize * 21, //width
					letterSize //height
				};

				//draw text explaining how to resume the game
				DrawTextRec(GetFontDefault(), "PRESS SPACE TO RESUME", resumeTextbox, letterSize, letterSpacing, true, GRAY);
			}

			void Menu()
			{
				const Rectangle quitTextbox =
				{
					windowWidth / 2 - letterSize * 9, //x position
					windowHeight / 1.80f, //y position
					letterSize * 26, //width
					letterSize //height
				};

				do
				{
					//Draw
					BeginDrawing();
					DrawRectangleRec(player::player1.body, player::player1.color); //draw player one's paddle
					DrawRectangleRec(player::player2.body, player::player2.color); //draw player one's paddle

					if (player::player1.score >= 10 || player::player2.score >= 10)
					{
						WinMenu();
					}
					else
					{
						PauseMenu();
					}
					//draw text explaining how to quit the game
					DrawTextRec(GetFontDefault(), "PRESS M TO GO BACK TO MENU", quitTextbox, letterSize, letterSpacing, true, GRAY);
					ClearBackground(BLACK);
					EndDrawing();
				} while (!IsKeyPressed(KEY_SPACE) && !IsKeyPressed(KEY_M));
			}
		}

		void Game()
		{
			const short letterSize = 20;
			ball::ball[0] = ball::Generate({ windowWidth / 2, windowHeight / 2 }, { 5, 5 });

			//start Game loop and keep it going until the users press the 'ESC' key
			while (!IsKeyPressed(KEY_ESCAPE) && player::player1.score < 10 && player::player2.score < 10)
			{
				UpdateMusicStream(pong::music);

				//Input & Update
				player::Move(player::player1);
				player::Move(player::player2);
				for (short i = 0; i < ball::activeBalls; i++)
				{
					ball::Move(ball::ball[i]);
				}
				//generate power up boxes, check if the ball collided with them and apply power ups
				powerups::Manager();
				for (short i = 0; i < ball::activeBalls; i++)
				{
					ball::ScorePoint(ball::ball[i]); //update scores if the ball moved beyond any player
				}

				//Draw
				BeginDrawing();
				//draw player one's score and winStreak
				DrawTextCodepoint(GetFontDefault(), ChangeIntNumberToCharNumber(player::player1.winStreak),
					{ windowWidth / 10, windowHeight / 10 * 2 }, 50, GRAY);
				DrawTextCodepoint(GetFontDefault(), ChangeIntNumberToCharNumber(player::player1.score),
					{ windowWidth / 2 - windowWidth / 20 * 1.5f, windowHeight / 2.15f }, 5, GREEN);
				//draw player two's score and winStreak
				DrawTextCodepoint(GetFontDefault(), ChangeIntNumberToCharNumber(player::player2.winStreak),
					{ windowWidth / 10 * 7, windowHeight / 10 * 2 }, 50, GRAY);
				DrawTextCodepoint(GetFontDefault(), ChangeIntNumberToCharNumber(player::player2.score),
					{ windowWidth / 2 + windowWidth / 20, windowHeight / 2.15f }, 5, GREEN);

				DrawRectangleRec(player::player1.body, player::player1.color); //draw player one's paddle
				DrawRectangleRec(player::player2.body, player::player2.color); //draw player two's paddle
				for (short i = 0; i < ball::activeBalls; i++)
				{
					DrawCircleV(ball::ball[i].position, ball::ball[i].radius, ball::ball[i].color); //draw ball
				}
				powerups::DrawPowerUpBox(); //draw power up box if present
				powerups::DrawPowerUpEffects(); //draw any active power up effect
				DrawLine(windowWidth / 2, 0, windowWidth / 2, windowHeight, GRAY); //draw line between each score
				DrawText("PRESS ESCAPE TO PAUSE", windowWidth / 2 - letterSize * 6, windowHeight / 15, letterSize, GRAY);
				ClearBackground(BLACK);
				EndDrawing();
			}

			ingame_menu::Menu();

			//if the game has ended, reset everything and add one point to the winner's win streak
			if (player::player1.score >= 10 || player::player2.score >= 10)
			{
				ball::DeInit();
				player::player1.score > player::player2.score ? player::player1.winStreak++ : player::player2.winStreak++;
				
				//if any win streak exceeds 9, reset both to 0, because DrawTextCodepoint supports only 1 char
				player::DeInit(player::player1.winStreak >= 10 || player::player2.winStreak >= 10);				
			}

			if (IsKeyPressed(KEY_SPACE))
			{
				Game();
			}

			//if any player returns to menu, reset everything
			ball::DeInit();
			player::DeInit(true);
		}
	}
}