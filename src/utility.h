#ifndef UTILITY_H
#define UTILITY_H

bool IntIsWithinRange(int input, int min, int max);
float GetAbsoluteValue(float number);
int InputKey();
char ChangeIntNumberToCharNumber(int number);

#endif // !UTILITY_H

