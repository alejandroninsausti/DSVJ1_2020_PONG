#include "utility.h"

#include "raylib.h"

bool IntIsWithinRange(int input, int min, int max)
{
	if (input < min || input > max)
	{
		return false;
	}
	else
	{
		return true;
	}
}

float GetAbsoluteValue(float number)
{
	//if the number is positive, return number, else, multiply by -1 and return the positive version
	if (number > 0)
	{
		return number;
	}
	else
	{
		return number * -1;
	}
}

bool IsALetter_Int(int input)
{
	if (IntIsWithinRange(input, (int)'a', (int)'z'))
	{
		return true;
	}
	else if (IntIsWithinRange(input, (int)'A', (int)'Z'))
	{
		return true;
	}
	else
	{
		return false;
	}
}

int ChangeLetterCase_Int(int input, bool LowerToUpper)
{
	if (IsALetter_Int(input))
	{
		short lowerUpperCaseDifference = (int)'a' - (int)'A';

		if (LowerToUpper)
		{
			if (input > (int)'Z')
			{
				input = input - lowerUpperCaseDifference;
			}
		}
		else
		{
			if (input < (int)'a')
			{
				input = input + lowerUpperCaseDifference;
			}
		}
	}	
	return input;
}

int InputKey()
{
	int key = GetKeyPressed();
	                      
	//if there are more than one [KEYORDER]s, match the key's value with 
	//the last [KEYORDER]'s value (this is used, for example, with the arrows, which have 
	//more than one value)
	while (GetKeyPressed() > 0)
	{
		key = GetKeyPressed();		
	}	

	//if the key pressed was a letter, make sure it was in upper case
	key = ChangeLetterCase_Int(key, true);

	//return the correct key value
	return key;	
}

char ChangeIntNumberToCharNumber(int number)
{
	int charCero = '0'; //get the ascii value of the char '0'
	return charCero + number; //return the char of the selected number
}